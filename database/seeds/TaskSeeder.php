<?php

use Illuminate\Database\Seeder;
use App\Task;
use Faker\Factory as Faker;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        for($i = 0; $i < 26 ; $i++)
        {
            $taskNew = new Task();
            $taskNew->project_id = $faker->numberBetween($min = 1, $max = 16);
            $taskNew->worker_id = $faker->numberBetween($min = 1, $max = 16);
            $taskNew->task_end = $faker->date($format = 'Y-m-d' , $min = 'now');
            $taskNew->task_description = $faker->text($maxNbChars = 150);
            $taskNew->save();
        }
    }
}

