<?php

use Illuminate\Database\Seeder;
use App\Worker;
use Faker\Factory as Faker;

class WorkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        for($i = 0; $i < 16 ; $i++)
        {
            $workerNew = new Worker();
            $workerNew->name = $faker->firstName;
            $workerNew->last_name = $faker->lastName;
            $workerNew->team_id = $faker->numberBetween($min = 1, $max = 4);
            $workerNew->save();
        }
    }
}
