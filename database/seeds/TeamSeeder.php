<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = ['Team 1', 'Team 2' , 'Team 3' , 'Team 4'];

        foreach($teams as $team)
        {
            $teamNew = new Team();
            $teamNew->name = $team;
            $teamNew->save();
        }
    }
}
