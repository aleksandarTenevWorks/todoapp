<?php

use Illuminate\Database\Seeder;
use App\Project;
use Faker\Factory as Faker;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;
        for($i = 0; $i < 16 ; $i++)
        {
            $projectNew = new Project();
            $projectNew->name = $faker->text($maxNbChars = 20);
            $projectNew->description = $faker->text($maxNbChars = 100);
            $projectNew->project_start = $faker->date($format = 'Y-m-d' , $min = 'now');
            $projectNew->project_end = $faker->date($format = 'Y-m-d' , $min = 'now');
            $projectNew->team_id = $faker->numberBetween($min = 1, $max = 4);
            $projectNew->save();
        }
    }
}
