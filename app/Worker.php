<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    public function team()
    {
        return $this->belongsTo(Team::class , 'team_id' , 'id');
    }
    public function task()
    {
        return $this->hasOne(Task::class , 'id' , 'worker_id');
    }
}
