<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function team()
    {
        return $this->belongsTo(Team::class , 'team_id' , 'id');
    }
    public function tasks()
    {
        return $this->hasMany(Task::class , 'project_id' , 'id');
    }
}
