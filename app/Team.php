<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function workers()
    {
        return $this->hasMany(Worker::class , 'team_id' , 'id');
    }
    public function project()
    {
        return $this->hasOne(Project::class , 'team_id' , 'id');
    }
}
