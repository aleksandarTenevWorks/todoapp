<?php

namespace App\Http\Controllers;
use App\Worker;
use App\Team;
use App\Project;
use App\Task;
use Carbon\Carbon;
use App\Http\Requests\ProjectRequest;
use App\Http\Requests\TaskRequest;


use Illuminate\Http\Request;

class ToDoAppController extends Controller
{
    public function toDoView()
    {        
        $projectCollection = Project::with('tasks.workers')->orderBy('project_end' , 'ASC' )->get(); 

        foreach($projectCollection as $project)
        {
            $date = Carbon::now();
            $finished = $project->project_end < $date->format('Y-m-d');
            if ($finished)
            {
                $project->finished = true;
                $project->save();

            } else if($project->tasks->count() > 0)
            {
                if($finished == false)
                {
                    $tasksFinished = true;
                    foreach($project->tasks as $task)
                    {
                        if($task->finished == false )
                        {
                            $tasksFinished = false;
                        } 
                    }
                    if($tasksFinished == true)
                    {
                        $project->finished = true;
                        $project->save();
                    } elseif($tasksFinished == false)
                    {
                        $project->finished = false;
                        $project->save();
                    }
                }
            } else {
                $project->finished = false;
                $project->save(); 
            }
        };
        return view('toDo', compact('task', 'projectCollection' , 'date'));
    }
    public function addTaskView($projectId)
    {
        $project = Project::where('id' , '=' , $projectId )->with('team.workers')->first();
        return view('addTask' , compact('project' , 'projectId'));
    }
    public function addTaskPost(TaskRequest $request) 
    {
        $taskNew = new Task();
        $taskNew->project_id = $request->projectId;
        $taskNew->task_description = $request->task;
        $taskNew->task_end = $request->date;
        $taskNew->worker_id = $request->worker;
        $taskNew->save();
        return redirect()->route('toDoApp');
    }
    public function addProjectView()
    {
        $team = Team::get();
        return view('addProject' , compact('team'));
    }
    public function addProjectPost(ProjectRequest $request) 
    {
        $projectNew = new Project();
        $projectNew->name = $request->projectName;
        $projectNew->description = $request->desc;
        $projectNew->project_end = $request->endDate;
        $projectNew->project_start = $request->startDate;
        $projectNew->team_id = $request->team;
        $projectNew->save();
        return redirect()->route('toDoApp');
    }
    public function updateProjectView($projectId)
    {
        $team = Team::get();
        $project = Project::where('id' , '=' , $projectId )->with('team')->first();
        return view('updateProject' , compact('project' , 'team'));
    }
    public function updateProjectPost(ProjectRequest $request) 
    {
        $projectNew = Project::find($request->projectId);
        $projectNew->name = $request->projectName;
        $projectNew->description = $request->desc;
        $projectNew->project_end = $request->endDate;
        $projectNew->project_start = $request->startDate;
        $projectNew->team_id = $request->team;
        $projectNew->save();
        return redirect()->route('toDoApp');
    }
    public function deleteProject($projectId)
    {
        Task::where('project_id' , $projectId)->delete();
        Project::destroy($projectId);
        return redirect()->route('toDoApp');
    }
    public function markTaskFinished($taskId)
    {
        $task = Task::find($taskId);
        if($task->finished == true){
            $task->finished = false;
            $task->save();
        } elseif($task->finished == false) {
            $task->finished = true;
            $task->save();
        }
        return redirect()->route('toDoApp');
    }
    public function updateTaskView($taskId , $project)
    {
        $project = Project::find($project);
        $workers = Worker::where('team_id' , $project->team_id)->get();
        $task = Task::find($taskId);
        return view('updateTask' , compact('task' , 'workers' , 'project'));
    }
    public function updateTask(TaskRequest $request)
    {
        $taskNew = Task::find($request->taskId);
        $taskNew->project_id = $request->projectId;
        $taskNew->task_description = $request->task;
        $taskNew->task_end = $request->date;
        $taskNew->worker_id = $request->worker;
        $taskNew->save();
        return redirect()->route('toDoApp');
    }
    public function deleteTask($taskId)
    {
        Task::destroy($taskId);
        return redirect()->route('toDoApp');
    }
}

