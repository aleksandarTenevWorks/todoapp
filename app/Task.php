<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function project()
    {
        return $this->belongsTo(Project::class , 'project_id' , 'id');
    }
    public function workers()
    {
        return $this->belongsTo(Worker::class , 'worker_id' , 'id');
    }
}
