@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-md-center align-items-start">
    <div class="col-md-4 project">
        <div class="projectContentWrapper">
        <img src="http://hasantezcan.com/images/raptiye.png" alt="pin" class="pin">
        <form action=" {{ route('addTaskPost') }}" method="post" class="addUpdateForm">
            <input type="hidden" value="{{ $projectId }}" name="projectId">
            <div class="form-group">
                <label for="task">Task</label>
                <input type="text" class="form-control" name="task" placeholder="Enter task">
                {{$errors->first('task')}}
            </div>
            <div class="form-group">
                <label for="date">Due Date</label>
                <input type="date" class="form-control" name="date">
                {{$errors->first('date')}}

            </div>
            <div class="form-group">
                <label for="worker">Worker</label>
                    <select class="form-control" name="worker">
                    @foreach($project->team->workers as $worker)
                        <option value="{{ $worker->id }}">{{ $worker->name }}</option>
                    @endforeach
                    </select>
                {{$errors->first('worker')}}
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {{ csrf_field() }}
        </form>
        <a href="{{ route('toDoApp') }}">Return to projects</a>
        </div>
        </div>
    </div>
</div>
@endsection