@extends('master')


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <a href="{{ route('addProjectView') }}" class="newProjectBtn"><img src="http://hasantezcan.com/images/raptiye.png"
                    alt="pin" class="pinAdd">Add new Project</a>
        </div>
    </div> <!-- END OF ROW -->
    <div class="row">
        @foreach($projectCollection as $project)
        <div class="col-md-4 project">
            <div class="projectContentWrapper" style="transform:rotate({{ rand(-4,3) }}deg)">
                <img src="http://hasantezcan.com/images/raptiye.png" alt="pin" class="pin">
                <div class="dropdown ">
                    <button class="btn btn-secondary dropdown-toggle projectOptionButton" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-edit"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a href="{{ route('addTaskView' , ['projectId' => $project->id ]) }}" class="dropdown-item">Add
                            new task</a>
                        <a href="{{ route('updateProjectView' , ['projectId' => $project->id ]) }}" class="dropdown-item @if($project->finished == 1) disabled @endif">Update
                            Project</a>
                        <a href="{{ route('deleteProject' , ['projectId' => $project->id ]) }}" class="dropdown-item">Delete
                            Project</a></span>
                    </div>
                </div>
                <h2 style="@if($project->finished == 1) color:green @endif">{{ $project->name }} <i class="fas fa-check check @if($project->finished == 1) checkGreen @endif"></i></h2>
                <span>{{ $project->description }}</span></br>
                <span >Due date:
                    @if($project->finished == 0) {{ $project->project_end }} @elseif($project->finished == 1)
                    Finished @endif </span><br><br>
                <ul class="list-group list-group-flush">
                    @foreach($project->tasks as $task)
                    <li class="list-group-item">
                        <a href="{{ route('markTaskFinished' , ['taskId' => $task->id]) }}" > @if($task->finished == true)  <i class="far fa-check-square"></i>  @elseif($task->finished == false) <i class="far fa-square"></i> @endif </a>
                        <a href="{{ route('updateTaskView' , ['taskId' => $task->id , 'project' => $project->id ]) }}"
                            class="taskUpdateBtn @if($task->finished == 1) disabled @endif">Update Task |</a>
                        <a href="{{ route('deleteTask' , ['taskId' => $task->id]) }}" class="taskUpdateBtn">Delete Task</a><br>
                        <span style="color:@if($task->finished == true)  green  @endif"> Task : {{
                            $task->task_description }} </span><br>
                        <span style="color:@if($task->finished == true)  green  @endif">Worker : {{
                            $task->workers->name }} Due Date: {{ $task->task_end }}</span>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

