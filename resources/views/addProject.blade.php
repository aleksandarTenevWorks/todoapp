@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-md-center align-items-start">
    <div class="col-md-4 project">
        <div class="projectContentWrapper">
        <img src="http://hasantezcan.com/images/raptiye.png" alt="pin" class="pin">
        <form action="{{ route('addProjectPost') }} " method="post" class="addUpdateForm">
            <div class="form-group">
                <label for="task">Project Name:</label>
                <input type="text" class="form-control" name="projectName" placeholder="Project Name">
                {{ $errors->first('projectName') }}
            </div>
            <div class="form-group">
                <label for="desc">Description</label>
                <input type="text" class="form-control" name="desc" placeholder="Enter short description">
                {{ $errors->first('desc') }}

            </div>
            <div class="form-group">
                <label for="startDate">Start Date</label>
                <input type="date" class="form-control" name="startDate" >
                {{ $errors->first('startDate') }}
            </div>
            <div class="form-group">
                <label for="endDate">End Date</label>
                <input type="date" class="form-control" name="endDate" >
                {{ $errors->first('endDate') }}
            </div>
            <div class="form-group">
                <label for="team">Team</label>
                    <select class="form-control" name="team">
                    @foreach($team as $team)
                        <option value="{{ $team->id }}">{{ $team->name }}</option>
                    @endforeach
                    </select>
                {{ $errors->first('team') }}
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {{ csrf_field() }}
        </form>
        <a href="{{ route('toDoApp') }}">Return to projects</a>
        </div>
        </div>
    </div>
</div>
@endsection