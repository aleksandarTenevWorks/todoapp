@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-md-center align-items-start">
    <div class="col-md-4 project">
        <div class="projectContentWrapper">
        <img src="http://hasantezcan.com/images/raptiye.png" alt="pin" class="pin">
        <form action="{{ route('updateProjectPost') }} " method="post" class="addUpdateForm">
        <input type="hidden" name="projectId" value="{{ $project->id }}">
            <div class="form-group">
                <label for="task">Project Name:</label>
                <input type="text" class="form-control" name="projectName" value="{{ $project->name }}" placeholder="Project Name">
                {{ $errors->first('projectName') }}
            </div>
            <div class="form-group">
                <label for="desc">Description</label>
                <input type="text" class="form-control" name="desc" value="{{ $project->description }}" placeholder="Enter short description">
                {{ $errors->first('desc') }}
            </div>
            <div class="form-group">
                <label for="startDate">Start Date</label>
                <input type="date" class="form-control" name="startDate" value="{{ $project->project_start }}" >
                {{ $errors->first('startDate') }}
            </div>
            <div class="form-group">
                <label for="endDate">End Date</label>
                <input type="date" class="form-control" name="endDate" value="{{ $project->project_end }}">
                {{ $errors->first('endDate') }}
            </div>
            <div class="form-group">
                <label for="team">Team</label>
                    <select class="form-control" name="team">
                    <option value="{{ $project->team->id }}" selected="selected">{{ $project->team->name }}</option>
                    @foreach($team as $team)
                        @if($project->team->id != $team->id)
                            <option value="{{ $team->id }}">{{ $team->name }}</option>
                        @endif
                    @endforeach
                    </select>
                {{ $errors->first('team') }}
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {{ csrf_field() }}
        </form>
        <a href="{{ route('toDoApp') }}">Return to projects</a>
        </div>
        </div>
    </div>
</div>
@endsection