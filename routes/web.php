<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('toDoApp')->name('toDoApp')->uses('ToDoAppController@toDoView');
Route::get('toDoApp/addTask/{projectId}')->name('addTaskView')->uses('ToDoAppController@addTaskView');
Route::post('toDoApp/addTask}')->name('addTaskPost')->uses('ToDoAppController@addTaskPost');
Route::get('toDoApp/addProject')->name('addProjectView')->uses('ToDoAppController@addProjectView');
Route::post('toDoApp/addProject')->name('addProjectPost')->uses('ToDoAppController@addProjectPost');
Route::get('toDoApp/updateProject/{projectId}')->name('updateProjectView')->uses('ToDoAppController@updateProjectView');
Route::post('toDoApp/updateProject')->name('updateProjectPost')->uses('ToDoAppController@updateProjectPost');
Route::get('toDoApp/deleteProject/{projectId}')->name('deleteProject')->uses('ToDoAppController@deleteProject');
Route::get('toDoApp/markTaskFinished/{taskId}')->name('markTaskFinished')->uses('ToDoAppController@markTaskFinished');
Route::get('toDoApp/updateTask/{taskId}/{project}')->name('updateTaskView')->uses('ToDoAppController@updateTaskView');
Route::post('toDoApp/updateTask')->name('updateTask')->uses('ToDoAppController@updateTask');
Route::get('toDoApp/deleteTask/{taskId}')->name('deleteTask')->uses('ToDoAppController@deleteTask');








